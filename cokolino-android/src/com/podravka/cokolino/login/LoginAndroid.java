package com.podravka.cokolino.login;

import org.json.JSONException;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

public class LoginAndroid implements ILogin {

	private Handler uiThread;
	private Context context;
	private boolean loginSuccess = false;
	
	private String url = "http://peaceful-ocean-5642.herokuapp.com/";
	
	private String name = "";
	private String fbID = "";
	private String fbUsername = "";
	private int points = 0;
	private int level = 1;
	
	public LoginAndroid(Context c) {
		context = c;
		uiThread = new Handler();
	}
	
	@Override
	public boolean tryToLogIn() {
		
		loginSuccess = false;
		
		// Start Facebook login
		Session.openActiveSession((Activity)context, true, new Session.StatusCallback() {
			
			// callback on session changed state
			@Override
			public void call(Session session, SessionState state, Exception exception) {

				Log.d("SESSION", session.toString());
				
				if(session.isOpened()){
					// request for /me API
					Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
						
						// callback after Graph API response with user object
						@Override
						public void onCompleted(GraphUser user, Response response) {
							Log.d("SESSION", "COMPLETED");
							
							if( user != null){
								loginSuccess = true;
								Log.d("USERNAME", user.getName());	
								Log.d("USERNAME", user.getId());	
								name = user.getName();
								fbID = user.getId();
								fbUsername = user.getUsername();
								
								RequestParams params = new RequestParams();
								params.put("id", fbID);
								params.put("fullName", name);
								params.put("username", fbUsername);
								sendGETRequest("getUserInfo", params);
							}
							else{
								Log.d("USERNAME", "null");
							}
						}
					});
					
				}
				
			}
		});
		
		return loginSuccess;
	}

	@Override
	public boolean isLoggedIn() {
		return loginSuccess;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getPoints() {
		return points;
	}

	@Override
	public int getLevel() {
		return level;
	}
	
	private void sendGETRequest(String urlPattern, RequestParams params){
		
		AsyncHttpClient client = new AsyncHttpClient();
		
		client.get(url + urlPattern, params, new JsonHttpResponseHandler(){
			public void onSuccess(org.json.JSONObject object) {
				try {
					if (object.getInt("success") == 1){
						Log.d("RESPONSE", "success");
						points = object.getInt("points");
						level = object.getInt("level");
						//Log.d("RESPONSE", String.valueOf(level));
						//Log.d("RESPONSE", String.valueOf(points));
					}
				} catch (JSONException e) {
					Log.d("RESPONSE", "fail");
					e.printStackTrace();
				}
			}
		});
	}


	@Override
	public void updateUserPoints(int newPoints) {
		RequestParams params = new RequestParams();
		params.put("id", fbID);
		params.put("newPoints", String.valueOf(newPoints));
		sendGETRequest("updateUserPoints/", params);
	}
	
	@Override
	public void updateUserLevel(int newLevel) {
		RequestParams params = new RequestParams();
		params.put("id", fbID);
		params.put("newLevel", String.valueOf(newLevel));
		sendGETRequest("updateUserLevel", params);
	}

	@Override
	public void refreshUserInfo() {
		RequestParams params = new RequestParams();
		params.put("id", fbID);
		if( loginSuccess == true )
			sendGETRequest("getUserInfo", params);
		else
			tryToLogIn();
	}

}
