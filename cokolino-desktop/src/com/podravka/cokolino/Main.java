package com.podravka.cokolino;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
//import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.podravka.cokolino.login.DesktopLogin;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "�okolino";
		cfg.useGL20 = true;
		cfg.width = 1024;
		cfg.height = 720;
//		TexturePacker2.process("C:\\Users\\Dario\\Desktop\\novo", "C:\\Users\\Dario\\Desktop\\novo\\result", "uiskin");
		
		new LwjglApplication(new MainGame(new DesktopLogin()), cfg);
	}
}
