package com.podravka.cokolino.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.podravka.cokolino.MainGame;
import com.podravka.cokolino.points.Points;

public class MainMenu extends AbstractScreen {	
	
	public static final String PLAY_TEXT = "igraj";
	public static final String ENTER_CODE_TEXT = "unesi kod";
	public static final String FREE_GAME_TEXT = "igraj za lino bodove";
	public static final String SHARE_TEXT = "pozovi prijatelje";
	public static final String POINTS_TEXT = "Vaši bodovi: ";

	Label userNameLabel;
	Label totalPoints;

	public MainMenu(MainGame game) {
		super(game);
	}
	
	@Override
	public void show() {
		super.show();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if( MainGame.loginManager.isLoggedIn() ){
			// name check
			if(!MainGame.loginManager.getName().equals(userNameLabel.getText()))
				userNameLabel.setText(MainGame.loginManager.getName());
			
			// points check
			if( (MainGame.loginManager).getPoints() != MainGame.points.getPoints()){
				MainGame.points.setPoints(MainGame.loginManager.getPoints());
				totalPoints.setText(POINTS_TEXT + MainGame.points.toString());
			}
			
			// level check
			if( MainGame.loginManager.getLevel() != MainGame.level)
				MainGame.level = MainGame.loginManager.getLevel();
		}
		
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		Table table = new Table( getSkin() );
		table.setFillParent(true);
		table.setBackground("main/main-menu-bg");
		table.defaults().size(385, 70).spaceBottom(10);
		table.center();
		
		TextButton startGameButton = new TextButton(PLAY_TEXT, getSkin());
		startGameButton.addListener(new InputListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.setScreen(new WorldMap(game));
			}
		});
		
		table.add(startGameButton).size(385, 170);
		table.row();
		
		totalPoints = new Label(POINTS_TEXT + MainGame.points.toString(), getSkin());
		TextButton codeInputButton = new TextButton(ENTER_CODE_TEXT, getSkin(), "small");

		userNameLabel = new Label("User", getSkin());

		codeInputButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				Gdx.input.getTextInput(new TextInputListener() {
					
					@Override
					public void input(String text) {
						int newPoints = MainGame.loginManager.getPoints() + 10;
						MainGame.loginManager.updateUserPoints(newPoints);
					}
					
					@Override
					public void canceled() {
						
					}
				}, "Unesite kod sa pakiranja", "");
			}
		});
		table.add(codeInputButton);
		table.row();
		
		TextButton freeGameButton = new TextButton(FREE_GAME_TEXT, getSkin(), "small");
		freeGameButton.addListener(new InputListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				MainGame.worldMapMode = false;
				game.setScreen(new BubbleMathGame(game));
			}
		});
		table.add(freeGameButton);
		table.row();
		
		TextButton inviteButton = new TextButton(SHARE_TEXT, getSkin(), "small");
//		table.add(inviteButton);
//		table.row();
		//table.add(inviteButton);
		//table.row();

		table.add(totalPoints).center();
		
		table.row();
		
		userNameLabel = new Label("User", getSkin());
		userNameLabel.setText(MainGame.loginManager.getName());
		table.add(userNameLabel).center();
		
		stage.addActor(table);
	}
}
