package com.podravka.cokolino.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.podravka.cokolino.MainGame;
import com.podravka.cokolino.physics.BoxObject;
import com.podravka.cokolino.physics.BoxObjectManager;
import com.podravka.cokolino.physics.BoxUserData;
import com.podravka.cokolino.utility.Position;

public class BubbleMathGame extends AbstractScreen {
	
	/** maximum number of digits a user can enter in the numpad */
	private static final int MAX_NUMPAD_LEN = 6;
	
	/** determines the period when the games becomes harder in milliseconds */
	private static final int GAME_PERIOD = 10000;
	
	/** determines how fast the bubble spawn rate drops after a period tick */
	private static final int DIFFICULTY_DROP = 10;
	
	/** maximum number of balloons that are allowed to fall before the game ends */
	private static final int MAX_LIFE = 10;
	
	/** minimum score needed to advance on the world map */
	private static final int MINIMUM_SCORE = 10;
	
	/** number that determines how often objects spawn (lower means faster) */
	private static int objectCountTimer = 150;
	
	private static boolean WORLD_MAP_MODE = false;
	
	/** true if the game has ended, false otherwise */
	private boolean gameOver;
	
	/** determines how much bubbles can fall to the ground */
	private int life;
	
	private long startTime;

	private World world;
//	private Box2DDebugRenderer renderer;
	private OrthographicCamera camera;
	
	private BoxObjectManager manager;
	
	private float height;
	private float width;
	
	private InputListener tableListener;
	private Label lifeLabel;
	private Label result;
	private Label score;
	
	/** floor object */
	private BoxObject floor;
	
	/** bubble objects will be stored here */ 
	private BoxObject o;
	private int count;
	
	public BubbleMathGame(MainGame game) {
		super(game);
		initGame();
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		drawUI();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		if(gameOver) {
			endGame();
			return;
		}
	
		spawnBubbles();
		
		manager.update(delta);
		
		reduceLife();
		
//		renderer.render(world, camera.combined);
		world.step(1/60f, 6, 2);
		
		getBatch().begin();
		manager.draw(getBatch());
		getBatch().end();
	}
	
	@Override
	public void dispose() {
		super.dispose();
		world.dispose();
	}
	
	/** Initialize game properties. */
	private void initGame() {
		
		height = Gdx.graphics.getHeight() / 5;
		width = Gdx.graphics.getWidth() / 5;
		
		world = new World(new Vector2(0, -5f), true);
		manager = new BoxObjectManager(world);
		
//		renderer = new Box2DDebugRenderer();
		
		camera = new OrthographicCamera(width, height);
		camera.position.set(width / 2f, height / 2f, 0);
		camera.update();
		
		count = objectCountTimer;
		startTime = System.currentTimeMillis();
	
		//floor object
		floor = new BoxObject(null, new Vector2(width / 2, 1), world, BodyType.StaticBody, 1, 
				BoxUserData.FLOOR, BoxObject.POLY_OBJECT, getSkin());
		
		manager.add(floor);

		world.setContactListener(manager);		
		createTableListener();
		
		life = MAX_LIFE;
		gameOver = false;
	}
	
	/** Draws the user interface for this game. */
	private void drawUI() {
		TextButton btn;
		
		Table backTable = new Table(getSkin());
		backTable.setFillParent(true);
		backTable.bottom().right();
		
		TextButton returnBtn = new TextButton("Povratak", getSkin());
		returnBtn.addListener(new InputListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if(!MainGame.worldMapMode)
					game.setScreen(new MainMenu(game));
				else
					game.setScreen(new WorldMap(game));
			}
		});
//		backTable.add(btn).width(300).height(100);
		
		Table table = new Table(getSkin());
		table.setFillParent(true);
		table.setBackground("balloon-game/balloon-game-bg");
		table.defaults().size(100, 100);
		
		
		result = new Label("0", getSkin(), "numpad");
		
		table.right();
		table.add(result).colspan(3).width(300).height(50).center();
		table.row();
		
		
		for (int i = 1; i <= 9; i++) {
			btn = new TextButton(String.valueOf(i), getSkin());
			btn.addListener(tableListener);
			table.add(btn);
			
			if(i % 3 == 0)
				table.row();
		}
		
		btn = new TextButton("0", getSkin());
		btn.addListener(tableListener);
		table.add(btn);
		
		btn = new TextButton("-", getSkin());
		btn.addListener(tableListener);
		table.add(btn);
		
		btn = new TextButton("C", getSkin());
		btn.addListener(tableListener);
		table.add(btn);
		
		table.row();
		
		table.add(returnBtn).colspan(3).width(300).height(100);
		
		stage.addActor(table);
		
		score = new Label("0", getSkin());
		lifeLabel = new Label("Zivot: " +  Integer.toString(life), getSkin());
		
		Table scoreTable = new Table(getSkin());
		scoreTable.setFillParent(true);
		scoreTable.bottom();
		scoreTable.add(new Label("Bodovi: ", getSkin()));
		scoreTable.add(score).width(100);
		scoreTable.add(lifeLabel).width(200);
		
		stage.addActor(scoreTable);
//		stage.addActor(backTable);
	}
	
	/** Spawns expression bubbles. */
	private void spawnBubbles() {
		if(count == objectCountTimer) {
			for (int i = 0; i < 1; i++) {
				
				o = new BoxObject(getSkin().getRegion("balloon-game/balloon"), randomSpawnPosition(), world, BodyType.DynamicBody, i, 
								BoxUserData.BUBBLE, BoxObject.CIRCLE_OBJECT, getSkin());
				manager.add(o);
			}
			count = 0;
			increaseDifficulty();
		}
		count++;
	}
	
	/** shows the end game text with the result and ends the game */
	private void endGame() {
		Label end = new Label("Kraj igre!\n", getSkin());
		stage.addActor(end);
		Position.setActorPosition(end,  303, 566);
		
		if(Integer.parseInt(score.getText().toString()) >= MINIMUM_SCORE) {
			game.advanceLevel();
			game.setScreen(new WorldMap(game));
		}
	}
	
	/** reduce player life after bubbles fall to the ground and sets the gameOver to true property if life reaches 0 */
	private void reduceLife() {
		life = MAX_LIFE - manager.getCollisionCount();
		lifeLabel.setText("Zivot: " + Integer.toString(life));
		
		if(life == 0) {
			gameOver = true;
		}
	}
	
	/** make the game harder after a periodic tick */
	private void increaseDifficulty() {
		if(System.currentTimeMillis() - startTime > GAME_PERIOD) {
			startTime = System.currentTimeMillis();
			objectCountTimer -= DIFFICULTY_DROP;
			if(objectCountTimer <= 0) {
				objectCountTimer = 10;
			}
		}
	}
	
	private Vector2 randomSpawnPosition() {
		Vector2 pos = new Vector2(width / 8, height + 10);
		
		pos.x += MainGame.random.nextFloat() * 100;
		pos.y += MainGame.random.nextFloat() * 2;
		
		return pos;
	}
	
	/** creates a listener for the numpad */
	private void createTableListener() {
		tableListener = new InputListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				TextButton btn = (TextButton) event.getListenerActor();
				if( btn.getText().toString().equals("C") )
					result.setText("");
				else if( btn.getText().toString().equals("-") ) {
					result.setText("-");
				} else {
					
					if(result.getText().toString().length() < MAX_NUMPAD_LEN) {
						result.setText(result.getText().toString() + btn.getText().toString());
					}
					
					if(manager.checkForResult(result.getText().toString())) {
						score.setText(String.valueOf(Integer.parseInt(score.getText().toString()) + 1));
						result.setText("");
					}
				}
			}
		};
	}
}
