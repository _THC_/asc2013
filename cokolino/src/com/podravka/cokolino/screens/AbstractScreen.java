package com.podravka.cokolino.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.podravka.cokolino.MainGame;

/**
 * The base class for all game screens.
 */
public abstract class AbstractScreen implements Screen {
    protected final MainGame game;
    protected final Stage stage;
    
    protected Texture background;

    private BitmapFont font;
    private SpriteBatch batch;
    private Skin skin;
    
    public AbstractScreen( MainGame game ) {
        this.game = game;
        this.stage = new Stage( 0, 0, true );
        game.loginManager.refreshUserInfo();
    }

    protected String getName() {
        return getClass().getSimpleName();
    }

    public BitmapFont getFont() {
        if( font == null ) {
            font = new BitmapFont(Gdx.files.internal("skins/comfortaa.fnt"), false);
        }
        return font;
    }

    public SpriteBatch getBatch() {
        if( batch == null ) {
            batch = new SpriteBatch();
        }
        return batch;
    }

    protected Skin getSkin() {
        if( skin == null ) {
            skin = new Skin( Gdx.files.internal( "skins/uiskin.json" ) );
        }
        return skin;
    }

    // Screen implementation

    @Override
    public void show() {
        Gdx.app.log( MainGame.LOG, "Showing screen: " + getName() );

        // set the input processor
        Gdx.input.setInputProcessor( stage );
    }

    @Override
    public void resize(int width, int height ) {
        Gdx.app.log( MainGame.LOG, "Resizing screen: " + getName() + " to: " + width + " x " + height );

        // resize and clear the stage
        stage.setViewport( width, height, true );
        stage.clear();
    }

    @Override
    public void render( float delta ) {
        // (1) process the game logic

        // update the actors
        stage.act( delta );

        // (2) draw the result

        // clear the screen with the given RGB color (black)
        Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

        // draw the actors
        stage.draw();
        
 	   if(Gdx.input.isTouched()) {
		      Gdx.app.log("KLIK MISEM", "x: " + Float.toString(Gdx.input.getX()) + " y: " + Float.toString(stage.getHeight() - Gdx.input.getY()));
	   }
        
        Table.drawDebug(stage);
    }

    @Override
    public void hide() {
        Gdx.app.log( MainGame.LOG, "Hiding screen: " + getName() );

        // dispose the resources by default
        dispose();
    }

    @Override
    public void pause() {
        Gdx.app.log( MainGame.LOG, "Pausing screen: " + getName() );
    }

    @Override
    public void resume() {
        Gdx.app.log( MainGame.LOG, "Resuming screen: " + getName() );
    }

    @Override
    public void dispose() {
        Gdx.app.log( MainGame.LOG, "Disposing screen: " + getName() );
        stage.dispose();
        if( font != null ) font.dispose();
        if( batch != null ) batch.dispose();
        if( skin != null ) skin.dispose();
    }
}