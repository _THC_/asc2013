package com.podravka.cokolino.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.OrderedMap;
import com.podravka.cokolino.MainGame;
import com.podravka.cokolino.utility.Position;

/**
 * Class that handles a Quiz style game
 */
public class QuizGame extends AbstractScreen {
	
	private static final float MAX_QUESTION_WIDTH = 800;
	
	private boolean endGame;
	
	/**
	 * Contains all available quizes
	 */
	private Array<Quiz> q;
	
	/**
	 * Contains a selected quiz from a list of available quizes
	 */
	private final Quiz quiz;
	
	/**
	 * Contains a collection of answers in Label format
	 */
	private ArrayList<Label> answers;
	
	private long startTime;
	
	private int countCorrectAnswers;
	
//	private final InputListener returnListener = new InputListener() {
//		@Override
//		public boolean touchDown(InputEvent event, float x, float y,
//				int pointer, int button) {
//			return true;
//		}
//		
//		@Override
//		public void touchUp(InputEvent event, float x, float y,
//				int pointer, int button) {
//			game.setScreen(new WorldMap(game));
//		}
//	};
	
	private final InputListener answerListener = new InputListener() {
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			return true;
		}
	
		@Override
		public void touchUp(InputEvent event, float x, float y,
				int pointer, int button) {
			
			if(endGame)
				return;
			
			Label ans = (Label) event.getListenerActor();
			if(quiz.getAnswers().get(ans.getText().toString())) {
				countCorrectAnswers++;
				
				if(countCorrectAnswers == quiz.getCorrectAnswerCount()) {
					Label correctAnswer = new Label("Točno! :)", getSkin());
					stage.addActor(correctAnswer);
					Position.setActorPosition(correctAnswer, 702, 375);
					
					game.advanceLevel();
					
					endGame = true;
					startTime = System.currentTimeMillis();
				}
				
				ans.setStyle(new LabelStyle(getFont(), 
							     new Color((float) 37.0/255, 
							    		   (float) 223.0/255, 
							    		   (float) 81.0/255, 
							    		   (float) 1)));
			} else {
				ans.setStyle(new LabelStyle(getFont(), 
							     new Color((float) 230.0/255, 
							    	       (float) 27.0/255, 
							    	       (float) 27.0/255, 
							    	       (float) 1)));
				
				Label wrongAnswer = new Label("Netočno :(", getSkin());
				stage.addActor(wrongAnswer);
				Position.setActorPosition(wrongAnswer, 702, 375);
				
				endGame = true;
				startTime = System.currentTimeMillis();
			}
		}
	};
	
	@SuppressWarnings("unchecked")
	public QuizGame(MainGame game) {
		super(game);
		endGame = false;
		countCorrectAnswers = 0;
		
		Json json = new Json();
		q = json.fromJson(Array.class, Gdx.files.internal("data/json/quizes.json").reader("UTF-8"));
		quiz = q.random();
		
		answers = new ArrayList<Label>();
		Set<String> keyset = quiz.getAnswers().keySet();
		Label answerLabel;
		for(String s : keyset) {
			answerLabel = new Label(s, getSkin());
			answerLabel.addListener(answerListener);
			answers.add(answerLabel);
		}
	}

	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		Table table = new Table(getSkin());
		table.setFillParent(true);
		table.setBackground("quiz/background_kviz");
		
		table.center();
		table.row();
		
		table.defaults().spaceBottom(50).left();
		
		int len = answers.size();
		for (int i = 0; i < len; i++) {
			answers.get(i).addListener(answerListener);
			table.add(new Label(Integer.toString(i+1) + ". ", getSkin()));
			table.add(answers.get(i)).width(385);
			table.row();
		}
		
		stage.addActor(table);
		
//		TextButton returnButton = new TextButton("Povratak", getSkin());
//		returnButton.addListener(returnListener);
//		returnButton.setSize(380, 100);
//		stage.addActor(returnButton);
//		Position.setActorPosition(returnButton, 50, 50);
		
		
		Table questionTable = new Table(getSkin());
		questionTable.setFillParent(true);
		questionTable.top();
		questionTable.defaults().padTop(100).spaceBottom(100).maxWidth(MAX_QUESTION_WIDTH);
		
		Label question = new Label(quiz.getQuestion().toUpperCase(), getSkin());

		questionTable.add(question).padTop(100).spaceBottom(100).width(question.getPrefWidth());
		
		questionTable.row();
		
		stage.addActor(questionTable);
	}
	
	@Override
	public void render(float delta) {		
		super.render(delta);
		
		if(endGame) {
			endQuiz();
		}
	}
	
	private void endQuiz() {
		if( System.currentTimeMillis() - startTime >= 2000 ) {
			game.setScreen(new WorldMap(game));
		}
	}
	
	private static class Quiz implements Serializable {
		private String question;
		private HashMap<String, Boolean> answers;

		public String getQuestion() {
			return question;
		}

		public HashMap<String, Boolean> getAnswers() {
			return answers;
		}
		
		public int getCorrectAnswerCount() {
			
			int count = 0;
			for(Boolean ans : this.answers.values()) {
				if(ans == true)
					count++;
			}
			
			return count;
		}

		@Override
		public void write(Json json) {
			Gdx.app.log(MainGame.LOG, "Writing JSON...");
			json.writeValue("question", question);
			json.writeObjectStart("answers");
			for (String s : answers.keySet()) {
				json.writeValue(s, answers.get(s));
			}
			json.writeObjectEnd();
		}

		@Override
		public void read(Json json, OrderedMap<String, Object> jsonData) {
			
			
			question = json.readValue("question", String.class, jsonData);
			
			@SuppressWarnings("unchecked")
			HashMap<String, Boolean> ans = json.readValue("answers", HashMap.class, Boolean.class, jsonData);
			
			this.answers = new HashMap<String, Boolean>();
			for (String s : ans.keySet()) {
				answers.put(s, ans.get(s));
			}
		}
	}
}
