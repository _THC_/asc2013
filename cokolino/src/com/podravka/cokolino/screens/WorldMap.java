package com.podravka.cokolino.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.podravka.cokolino.MainGame;
import com.podravka.cokolino.points.Points;
import com.podravka.cokolino.utility.Position;

public class WorldMap extends AbstractScreen {
	
	public static WorldMap worldMap;
	
	private static final float BUTTON_SIZE_X = 96f;
	private static final float BUTTON_SIZE_Y = 96f;
	
	private static final String RANDOM_TAG = "random";
	private static final String QUIZ_TAG = "quiz";
	private static final String BUBBLE_MATH_TAG = "bubble-math";
	private static final String BUTTON_DISABLED = "disabled";
	
	// world map level configuration
	private static final String[] conf = { QUIZ_TAG, 
										   QUIZ_TAG, 
										   QUIZ_TAG, 
										   BUBBLE_MATH_TAG, 
										   QUIZ_TAG, 
										   QUIZ_TAG,
										   BUBBLE_MATH_TAG,
										   QUIZ_TAG,
										   BUBBLE_MATH_TAG,
										   RANDOM_TAG
	};
	
	//coordinates are from a 1024*720 screen 
	private static final float[] btnX = { 129, 260, 167, 406, 498, 710, 660, 640, 873, 911 };
	private static final float[] btnY = { 588, 471, 298, 257, 490, 498, 294,  74,  98, 340 };
	
	private final InputListener quizListener = new GameButtonListener() {
		@Override
		public void touchUp(InputEvent event, float x, float y,
				int pointer, int button) {
			handle(new QuizGame(game));
		}
	};
	
	private final InputListener balloonGameListener = new GameButtonListener() {
		@Override
		public void touchUp(InputEvent event, float x, float y,
				int pointer, int button) {
			handle(new BubbleMathGame(game));
		}
	};

	public WorldMap(MainGame game) {
		super(game);
		worldMap = this;
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		Table table = new Table(getSkin());
		table.setFillParent(true);
		table.setBackground("main/world-map");
		table.bottom().left();
		
		TextButton returnButton = new TextButton("Povratak", getSkin());
		returnButton.addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				game.setScreen(new MainMenu(game));
			}
		});
		table.add(returnButton).size(385, 100);
		
		stage.addActor(table);
		
		generateWorld();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
	}
	
	private void generateWorld() {
		
		for (int i = 0; i < MainGame.level; i++) {
			stage.addActor(createGameButton(btnX[i], btnY[i], conf[i]));
		}
		
		for(int i = MainGame.level; i < conf.length; i++) {
			stage.addActor(createGameButton(btnX[i], btnY[i], BUTTON_DISABLED));
		}
	}
	private Button createGameButton(float x, float y, String buttonType) {
		Button btn = new Button(getSkin(), buttonType);
		
		if(buttonType.equals(RANDOM_TAG)) {
			buttonType = conf[MainGame.random.nextInt(conf.length)];
		}
		
		x = Position.convertWidth(x, stage.getWidth());
		y = Position.convertHeight(y, stage.getHeight());
		btn.setBounds(x - BUTTON_SIZE_X / 2, y - BUTTON_SIZE_Y / 2, BUTTON_SIZE_X, BUTTON_SIZE_Y);
		
		if(buttonType.equals(QUIZ_TAG)) {
			btn.addListener(quizListener);
		}
		
		if(buttonType.equals(BUBBLE_MATH_TAG)) {
			btn.addListener(balloonGameListener);
		}
		
		return btn;
	}
	
	/**
	 * Listener for the game buttons on the world map
	 * @author Dario
	 *
	 */
	private class GameButtonListener extends InputListener {
		
		private boolean canEnterGame() {
			if( MainGame.points.getPoints() < Points.GAME_COST )
				return false;
			
			return true;
		}
		
		private void displayNotEnoughPoints() {
			Label points = new Label("Nemaš dovoljno bodova :(", getSkin());
			stage.addActor(points);
			Position.setActorPosition(points, 56, 145);
		}
		
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			return true;
		}
		
		public void handle(AbstractScreen screen) {
			if(canEnterGame()) {
				
				MainGame.worldMapMode = true;
				
				game.setScreen(screen);
				MainGame.points.payGame();
			} else {
				displayNotEnoughPoints();
			}
		}
	}
}
