package com.podravka.cokolino;

import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.podravka.cokolino.login.ILogin;
import com.podravka.cokolino.points.Points;
import com.podravka.cokolino.screens.MainMenu;

public class MainGame extends Game {
	
	public static final int MAX_LEVEL = 10;
	
	
	public static final String LOG = MainGame.class.getSimpleName();
	public static ILogin loginManager;
	
	public static boolean worldMapMode = false;
	public static Random random = new Random();
	public static Points points;
	public static int level = 1;
	
	private FPSLogger fpsLogger;
	
	public MainGame() {
	}
	
	public MainGame(ILogin manager) {
		loginManager = manager;
	}
	
	@Override
	public void create() {		
		Gdx.app.log(LOG, "Creating game...");
		fpsLogger = new FPSLogger();
		MainGame.points = new Points();
		
		loginManager.tryToLogIn();
		
		setScreen(new MainMenu(this));
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {		
		super.render();
		fpsLogger.log();	
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	
	public void advanceLevel() {
		loginManager.updateUserLevel(++level);
		
		if(level > MAX_LEVEL)
			level = MAX_LEVEL;
	}
}
