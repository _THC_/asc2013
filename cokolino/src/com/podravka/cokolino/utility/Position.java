package com.podravka.cokolino.utility;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class Position {
	
	//referent screen size for displaying actors
	private static final float REFERENCED_WIDTH = 1024f;
	private static final float REFERENCED_HEIGHT = 720f;
	
	/**
	 * Converts the given coordinate to actual coordinate on the screen
	 * @param x coordinate on the referenced screen size
	 * @param screen actual screen size
	 * @return
	 */
	public static float convertWidth(float x, float width) {
		return (x / REFERENCED_WIDTH) * width;
	}
	
	/**
	 * Converts the given coordinate to actual coordinate on the screen
	 * @param x coordinate on the referenced screen size
	 * @param screen actual screen size
	 * @return
	 */
	public static float convertHeight(float y, float height) {
		return (y / REFERENCED_HEIGHT) * height;
	}
	
	public static void setActorPosition(Actor actor, float x, float y) {
		actor.setPosition(convertWidth(x, actor.getStage().getWidth()), convertHeight(y, actor.getStage().getHeight()));
	}
}
