package com.podravka.cokolino.login;

public interface ILogin {
	
	public boolean tryToLogIn();
	public boolean isLoggedIn();
	public int getPoints();
	public int getLevel();
	public String getName();
	
	public void updateUserPoints(int newPoints);
	public void updateUserLevel(int newLevel);
	public void refreshUserInfo();
}
