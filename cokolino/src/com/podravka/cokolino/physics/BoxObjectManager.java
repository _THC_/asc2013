package com.podravka.cokolino.physics;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Manager that handles the World and all the Box2D objects in its world including collisions
 * @author Dario
 *
 */
public class BoxObjectManager implements ContactListener {
	
	private World world;
	
	private ArrayList<BoxObject> objects;
	private ArrayList<BoxObject> deleteList;
	
	private int countCollisions;
	
	public BoxObjectManager(World world) {
		this.world = world;
		objects = new ArrayList<BoxObject>();
		deleteList = new ArrayList<BoxObject>();
		
		world.setContactListener(this);
		
		countCollisions = 0;
	}
	
	public BoxObject add(BoxObject o) {
		objects.add(o);
		return o;
	}
	
	/**
	 * Draws all the textures for all the Box2D objects in the list.
	 * @param batch
	 */
	public void draw(SpriteBatch batch) {
		int len = objects.size();
		for (int i = 0; i < len; i++) {
			objects.get(i).draw(batch);
		}
	}
	
	/**
	 * First it deletes all the objects in the delete list. After the deletion it updates the positions of all
	 * the textures and Box2D objects for every Box2D object in the list.
	 * @param delta
	 */
	public void update(float delta) {
		int len = deleteList.size();
		
		for (int i = 0; i < len; i++) {
			deleteList.get(i).destroy(world);	
		}
		deleteList.clear();
		
		len = objects.size();
		for (int i = 0; i < len; i++) {
			objects.get(i).update(delta);
		}
	}

	public void addForDeletion(BoxObject o) {
		deleteList.add(o);
	}
	
	/**
	 * Checks for if there is a correct answer
	 * @param r
	 * @return
	 */
	public boolean checkForResult(String r) {
		boolean found = false;
		int result = Integer.parseInt(r);
		
		int len = objects.size();
		for (int i = 0; i < len; i++) {
			
			if(objects.get(i).getTextureWrapper() instanceof BalloonTextureWrapper ) {
				BalloonTextureWrapper w = (BalloonTextureWrapper) objects.get(i).getTextureWrapper();
				if( result == w.getResult() ) {
					w.invalidateResult();
					this.addForDeletion(objects.get(i));
					found = true;
				}
			}
		}
		
		return found;
	}
	
	public int getCollisionCount() {
		return countCollisions;
	}
	
	@Override
	public void beginContact(Contact contact) {
		
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		
		Body bodyA = fixtureA.getBody();
		Body bodyB = fixtureB.getBody();
		
		BoxUserData userDataA = (BoxUserData) bodyA.getUserData();
		BoxUserData userDataB = (BoxUserData) bodyB.getUserData();
		
		if( userDataA.getCollisionGroup() == BoxUserData.BUBBLE && userDataB.getCollisionGroup() == BoxUserData.FLOOR ) {
			handleContact((BoxObject) userDataA.getBoxObject());
		} else if (userDataA.getCollisionGroup() == BoxUserData.FLOOR && userDataB.getCollisionGroup() == BoxUserData.BUBBLE) {
			handleContact((BoxObject) userDataB.getBoxObject());
		}
	}
	
	public void handleContact(BoxObject o) {
		this.addForDeletion(o);
		countCollisions++;
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
	}
}
