package com.podravka.cokolino.physics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.podravka.cokolino.MainGame;

/**
 * Texture wrapper class specific for Box2D balloons in the balloon game
 * @author Dario
 *
 */
public class BalloonTextureWrapper extends TextureWrapper {
	
	private static final int INVALID_RESULT = -1000000000;
	
	//maximum number an expression will contain
	private static final int EASY_EXPRESSION = 15;
	private static final int MEDIUM_EXPRESSION = 25;
	private static final int HARD_EXPRESSION = 50;
	
	//offset from which an expression number will be generated
	private static final int EASY_OFFSET = 0;
	private static final int MEDIUM_OFFSET = 10;
	private static final int HARD_OFFSET = 20;
	
	private Label label;
	private char[] operators;
	
	private int result;
	
	public BalloonTextureWrapper(TextureRegion region, Vector2 pos, Skin skin) {
		super(region, pos);
		
		operators = new char[3];
		operators[0] = '+';
		operators[1] = '-';
		operators[2] = 'x';
		
		label = new Label(createEasyExpression(), skin);
		label.setPosition(pos.x - label.getWidth() / 2, pos.y - label.getHeight() / 2);
	}

	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		label.setPosition(x - label.getWidth() / 2, y - label.getHeight());
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		super.draw(batch);
		label.draw(batch, 1);
	}
	
	private String createExpression(int n, int offset) {
		StringBuilder sb = new StringBuilder();
		
		Integer operand1 = MainGame.random.nextInt(n) + offset;
		Character operator = operators[MainGame.random.nextInt(operators.length)];
		Integer operand2 = MainGame.random.nextInt(n) + offset;
		
		this.result = solveExpression(operand1, operand2, operator);
		
		sb.	append(operand1.toString()).append(" ").
			append(operator.toString()).append(" ").
			append(operand2.toString());
		
		return sb.toString();
	}
	
	private String createExpression(int n) {
		return createExpression(n, 0);
	}
	
	private String createRandomExpression() {
		return createExpression(100);
	}
	
	private String createEasyExpression() {
		return createExpression(EASY_EXPRESSION, EASY_OFFSET);
	}
	
	private String createMediumExpression() {
		return createExpression(MEDIUM_EXPRESSION, MEDIUM_OFFSET);
	}
	
	private String createHardExpresson() {
		return createExpression(HARD_EXPRESSION, HARD_OFFSET);
	}
	
	private int solveExpression(Integer operand1, Integer operand2, char op) {
		int result = 0;
		
		switch (op) {
		case '+':
			result = operand1 + operand2;
			break;
		case '-':
			result = operand1 - operand2;
			break;
		case 'x':
			result = operand1 * operand2;
			break;
		}
		
		return result;
	}
	
	public int getResult() {
		return result;
	}
	
	public void invalidateResult() {
		result = INVALID_RESULT;
	}
}
