package com.podravka.cokolino.physics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Texture wrapper class that draws and manages sprites for Box2D objects
 * @author Dario
 *
 */
public class TextureWrapper {

	private TextureRegion region;
	
	private int width;
	private int height;
	private Vector2 position;
	
	private float scaleX;
	private float scaleY;
	private float originX;
	private float originY;
	private float rotation;
	
	public TextureWrapper(int width, int height, TextureRegion region, Vector2 pos) {
		this.position = pos;
		this.region = region;
		this.width = width;
		this.height = height;
		originX = width / 2;
		originY = height / 2;
		scaleX = 1;
		scaleY = 1;
	}
	
	public TextureWrapper(TextureRegion region, Vector2 pos) {
		this.position = pos;
		setTextureRegion(region);
	}
	
	public void setTextureRegion(TextureRegion region) {
		this.region = region;
		width = region.getRegionWidth();
		height = region.getRegionHeight();
		originX = width / 2;
		originY = height / 2;
		scaleX = 1;
		scaleY = 1;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setPosition(float x, float y) {
		position.set(x, y);
	}
	
	public void setRotation(float r) {
		rotation = r;
	}
	
	public void draw(SpriteBatch batch) {
		if(region != null)
			batch.draw(region, position.x - width / 2, position.y - height / 2, originX, originY, width, height, scaleX, scaleY, rotation);
	}
}
