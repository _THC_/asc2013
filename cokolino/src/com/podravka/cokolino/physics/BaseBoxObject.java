package com.podravka.cokolino.physics;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class BaseBoxObject {
	static final float WORLD_TO_BOX = 0.1f;
	static final float BOX_TO_WORLD = 10f;
	
	protected Body body;
	protected BoxUserData userData;
	protected Vector2 worldPosition;
	
	protected boolean isActive;
	
	public BaseBoxObject(Vector2 pos, World world, BodyDef.BodyType bodyType, int boxIndex, int collisionGroup){
		userData = new BoxUserData(this, boxIndex, collisionGroup);
		worldPosition = new Vector2();
		createBody(world, pos, bodyType, 0);     
		body.setUserData(userData);
		isActive = true;
    }
	
	protected float convertToBox(float x) {
		return x * WORLD_TO_BOX;
	}
	
	protected float convertToWorld(float x) {
       return x * BOX_TO_WORLD;
	}

	private void createBody(World world, Vector2 pos, BodyType bodyType, float angle ) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(pos.x, pos.y);
		bodyDef.angle = angle;
		
//		Gdx.app.log("Body", String.valueOf(pos.x) + " , " + String.valueOf(pos.y));
		
		this.body = world.createBody(bodyDef);
	}
	
	protected void makeRectFixture(float width, float height, float density, float restitution) {
		PolygonShape shape = new PolygonShape();
		
//		Gdx.app.log("Texture", String.valueOf(width) + " x " + String.valueOf(height));
		
		shape.setAsBox(convertToBox(width), convertToBox(height));
		
		FixtureDef fixture = new FixtureDef();
		fixture.shape = shape;
		fixture.density = density;
		fixture.restitution = restitution;
		
		body.createFixture(fixture);
		shape.dispose();
	}
	
	protected void makeCircleFixture(float radius, float density, float restitution) {
		FixtureDef fixture = new FixtureDef();
		fixture.shape = new CircleShape();
		fixture.shape.setRadius(convertToBox(radius));
		fixture.density = density;
		fixture.restitution = restitution;
		
		body.createFixture(fixture);
		fixture.shape.dispose();
	}
	
	/**
	 * Updates the position in the world for this Box2D object
	 */
	public void updateWorldPosition() {
		worldPosition.set(convertToWorld(body.getPosition().x), convertToWorld(body.getPosition().y));
	}
	
	public void destroy(World world) {
		if(isActive) {
			world.destroyBody(body);
			isActive = false;
		}
	}
}
