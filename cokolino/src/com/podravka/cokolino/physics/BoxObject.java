package com.podravka.cokolino.physics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class BoxObject extends BaseBoxObject {

	public static final int CIRCLE_OBJECT = 1;
	public static final int POLY_OBJECT = 2;
	
	private TextureWrapper texture;
	
	public BoxObject(TextureRegion region, Vector2 pos, World world, BodyType bodyType, int boxIndex,
			int collisionGroup, int shape, Skin skin) {
		super(pos, world, bodyType, boxIndex, collisionGroup);
		
		switch (shape) {
		case CIRCLE_OBJECT:
			texture = new BalloonTextureWrapper(region, pos, skin);
			makeCircleFixture(texture.getWidth(), 1, 0.8f);
			break;
		case POLY_OBJECT:
			texture = new TextureWrapper(1000, 100, region, pos);
			makeRectFixture(texture.getWidth() + 100, 100, 1, 1);
			break;
		default:
			break;
		}
	}

	/**
	 * Draws the textures for the Box2D object in their current position
	 * @param batch
	 */
	public void draw(SpriteBatch batch) {
		if(isActive)
			texture.draw(batch);
	}
	
	/**
	 * Updates the position in the world for this Box2D object and updates the position of its textures
	 * @param delta
	 */
	public void update(float delta) {
		
		updateWorldPosition();
		
		texture.setPosition(worldPosition.x / 2, worldPosition.y / 2);
		texture.setRotation((float) Math.toDegrees(body.getAngle()));
	}
	
	public TextureWrapper getTextureWrapper() {
		return texture;
	}
}
