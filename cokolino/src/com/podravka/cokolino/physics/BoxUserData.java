package com.podravka.cokolino.physics;

/**
 * User data class for all Box2D objects
 * @author Dario
 *
 */
public class BoxUserData {
	private int collisionGroup;
	private int boxId;
	
	private BaseBoxObject boxObject;
	
	public static final int BUBBLE = 1;
	public static final int FLOOR = 2;
	
	public BoxUserData(BaseBoxObject o, int boxId, int collisionGroup) {
		Set(boxId, collisionGroup);
		this.boxObject = o;
	}
	
    public void Set(int boxid,int collisionGroup){
        this.boxId = boxid;
        this.collisionGroup = collisionGroup;
    }
    
    public int getBoxId() {
    	return this.boxId;
    }
    
    public int getCollisionGroup() { 
    	return this.collisionGroup;
    }
    
    public BaseBoxObject getBoxObject() {
    	return boxObject;
    }
}
