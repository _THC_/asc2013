package com.podravka.cokolino.points;

import com.podravka.cokolino.MainGame;
import com.podravka.cokolino.screens.MainMenu;

public class Points {
	
	public static final int GAME_COST = 1;
	
	private int points;
	
	public Points() {
		points = 0;
	}
	
	public void addPoints(int points) {
		this.points += points;
	}
	
	/** pays the cost for entering a game on the world map */
	public void payGame() {
		int newPoints = points - GAME_COST;
		MainGame.loginManager.updateUserPoints(newPoints);
		setPoints(newPoints);
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}
	
	@Override
	public String toString() {
		return String.valueOf(points);
	}
}
